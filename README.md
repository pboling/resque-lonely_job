# resque-lonely_job

A resque plugin that ensures that only one job for a given queue will be running on any worker at a given time.